#ifndef clox_object_h
#define clox_object_h

#include "chunk.h"
#include "common.h"
#include "table.h"
#include "value.h"

#define OBJ_TYPE(value) (objType(AS_OBJ(value)))

#define IS_BOUND_METHOD(value) isObjType(value, OBJ_BOUND_METHOD)
#define IS_CLASS(value) isObjType(value, OBJ_CLASS)
#define IS_CLOSURE(value) isObjType(value, OBJ_CLOSURE)
#define IS_FUNCTION(value) isObjType(value, OBJ_FUNCTION)
#define IS_INSTANCE(value) isObjType(value, OBJ_INSTANCE)
#define IS_NATIVE(value) isObjType(value, OBJ_NATIVE)
#define IS_STRING(value) isObjType(value, OBJ_STRING)

#define AS_BOUND_METHOD(value) ((ObjBoundMethod *)AS_OBJ(value))
#define AS_CLASS(value) ((ObjClass *)AS_OBJ(value))
#define AS_CLOSURE(value) ((ObjClosure *)AS_OBJ(value))
#define AS_FUNCTION(value) ((ObjFunction *)AS_OBJ(value))
#define AS_INSTANCE(value) ((ObjInstance *)AS_OBJ(value))
#define AS_NATIVE(value) (((ObjNative *)AS_OBJ(value))->function)
#define AS_STRING(value) ((ObjString *)AS_OBJ(value))
#define AS_CSTRING(value) (((ObjString *)AS_OBJ(value))->chars)

typedef enum {
  OBJ_BOUND_METHOD,
  OBJ_CLASS,
  OBJ_CLOSURE,
  OBJ_FUNCTION,
  OBJ_INSTANCE,
  OBJ_NATIVE,
  OBJ_STRING,
  OBJ_UPVALUE
} ObjType;

struct sObj {
#ifdef POINTER_TAGGING
  struct sObj *_next;
#else
  ObjType type;
  bool isMarked;
  struct sObj *next;
#endif
};

typedef struct {
  Obj obj;
  int arity;
  int upvalueCount;
  Chunk chunk;
  ObjString *name;
} ObjFunction;

typedef Value (*NativeFn)(int argCount, Value *args);

typedef struct {
  Obj obj;
  NativeFn function;
} ObjNative;

struct sObjString {
  Obj obj;
  int length;
  char *chars;
  uint32_t hash;
};

typedef struct sUpvalue {
  Obj obj;
  Value *location;
  Value closed;
  struct sUpvalue *next;
} ObjUpvalue;

typedef struct {
  Obj obj;
  ObjFunction *function;
  ObjUpvalue **upvalues;
  int upvalueCount;
} ObjClosure;

typedef struct sObjClass {
  Obj obj;
  ObjString *name;
  Table methods;
} ObjClass;

typedef struct {
  Obj obj;
  ObjClass *klass;
  Table fields;
} ObjInstance;

typedef struct {
  Obj obj;
  Value receiver;
  ObjClosure *method;
} ObjBoundMethod;

ObjBoundMethod *newBoundMethod(Value receiver, ObjClosure *method);
ObjClass *newClass(ObjString *name);
ObjClosure *newClosure(ObjFunction *function);
ObjFunction *newFunction(void);
ObjInstance *newInstance(ObjClass *klass);
ObjNative *newNative(NativeFn function);
ObjString *takeString(char *chars, int length);
ObjString *copyString(const char *chars, int length);
ObjUpvalue *newUpvalue(Value *slot);
void printObject(Value value);

#ifdef POINTER_TAGGING

static inline ObjType objType(Obj *object) {
  const uintptr_t nextAsInt = (uintptr_t)object->_next;
  return (nextAsInt & 0xfull) >> 1ull;
}

static inline bool isObjType(Value value, ObjType type) {
  return IS_OBJ(value) && OBJ_TYPE(value) == type;
}

static inline bool isObjMarked(Obj *object) {
  const uintptr_t nextAsInt = (uintptr_t)object->_next;
  return nextAsInt & 1ull;
}

static inline void markObj(Obj *object) {
  object->_next = (struct sObj *)(((uintptr_t)object->_next) | 1ull);
}

static inline void unmarkObj(Obj *object) {
  object->_next = (struct sObj *)(((uintptr_t)object->_next) & ~1ull);
}

static inline struct sObj *objNext(Obj *object) {
  return (struct sObj *)(((uintptr_t)object->_next) & ~0xfull);
}

static inline void setObjNext(Obj *object, struct sObj *next) {
  object->_next = (struct sObj *)(((uintptr_t)next) |
                                  (((uintptr_t)object->_next) & 0xfull));
}

#else

static inline ObjType objType(Obj *object) { return object->type; }

static inline bool isObjType(Value value, ObjType type) {
  return IS_OBJ(value) && OBJ_TYPE(value) == type;
}

static inline bool isObjMarked(Obj *object) { return object->isMarked; }

static inline void markObj(Obj *object) { object->isMarked = true; }

static inline void unmarkObj(Obj *object) { object->isMarked = false; }

static inline struct sObj *objNext(Obj *object) { return object->next; }

static inline void setObjNext(Obj *object, struct sObj *next) {
  object->next = next;
}

#endif

#endif
